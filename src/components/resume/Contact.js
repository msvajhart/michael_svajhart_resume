import React from "react";
import styles from './css/contact.module.css';

const Contact = () => {
    return(
        <section className={styles.contact} id="contact">
            <h1>
                Contact Me
            </h1> 
        </section>
    );
}

export default Contact;