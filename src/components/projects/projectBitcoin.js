import React from "react";
import styles from './css/projectBitcoin.module.css';

const ProjectBitcoin = () => {

    const [loading, setLoading] = React.useState(true);
    const [data, setData] = React.useState(null);

    React.useEffect( ()=>{
        fetchBitcoinData();
    },[])

    const fetchBitcoinData = async() => {
        setLoading(true)
        let res = await fetch("https://api.coindesk.com/v1/bpi/currentprice.json");
        let d = await res.json();
        setData(d);
        setLoading(false)
    }

    return (
        <>
         <section className={styles.section}>
            <div className={styles.container}>
                <h1 className={styles.title}>Project Bitcoin</h1>
                <h6 className={styles.description}>API request to fetch bitcoin information.</h6>
                <button onClick={fetchBitcoinData}>Refresh Data</button>
                {
                    loading ? <Loading/> : <BitcoinInfo data={data}/>
                }
            </div>
            Bitcoin
         </section>
        </>
    )
}

const BitcoinInfo = ({data}) => {

    React.useEffect(()=>{console.log(data)},[data])

    if(data === null) return null;

    return (
        <div>
            <div>
                {data.bpi.USD.rate_float} {data.bpi.USD.symbol} {data.bpi.USD.description}
            </div>
            <div>{data.chartName}</div>
            <div>{data.disclaimer}</div>
            <div>{new Date(data.time.updated).toTimeString()}</div>
        </div>
    )

};

const Loading = () => {
    return <span> spinning wheel....</span> 
}

export default ProjectBitcoin;